     _         _                            _       _      
 ___| |_ _   _| | ___   _ __ ___   ___   __| |_   _| | ___ 
/ __| __| | | | |/ _ \ | '_ ` _ \ / _ \ / _` | | | | |/ _ \
\__ \ |_| |_| | |  __/_| | | | | | (_) | (_| | |_| | |  __/
|___/\__|\__, |_|\___(_)_| |_| |_|\___/ \__,_|\__,_|_|\___|
         |___/          A skinning system for Drupal Themes

Style.module is a skinning system that overrides stylesheets in themes.

===========================
  File structure
===========================
    In every module and theme, the /styles folder includes additional
    styles.

    Every directory in /styles is a different, unique, style.

    When a style is chosen, the style system replaces normal stylesheets
    for a theme or module with the styles own.
  
    It does this by replacing files relative to the original theme.
    
    Let's take an example, here's the output of DRUPAL_THEME_DIR/themes/moo/:
    
  +-DRUPAL_THEME_DIR/themes/moo/
  |
  +-moo.css
  |
  +-template.php
  |
  +-moo.info
  |
  +-page.tpl.php
  |
  +-images/
  | |
  | +-header-bg.png
  |
  +-logo.png
  |
  +-styles/
    |
    +-georgia-font/
    | |
    | +-moo.css
    |
    +-ocean/
    | |
    | +-moo.css
    | |
    | +-images/
    |   |
    |   +-background.png
    |
    +-day/
    | |
    | +-moo.css
    | |
    | +-images/
    | | |
    | | +-header.png
    | |
    | +-logo.png
    |
    +-night/
      |
      +-moo.css
      |
      +-images/
      | |
      | +-header-bg.png
      |
      +-logo.png

  If no style chosen, the theme will act normally.
  
  Day and Night styles
    the moo.css in the style overrides the theme's
    moo.css uses a the custom header-bg.png
    Logo.png is replaced by the same
  
  Georgia-font
    Uses moo.css with a modification of the site's fonts
    
  Ocean
    Custom moo.css with added background.png
  
  
  # See Tips: Theme stylesheet architecture
    
    =================
      Optionally: Files dir
    =================
      Color.module users rejoice. Styles.module supports all generated images
      and stylesheets natively by utilizing the files.
      
      This system treats the files directory as a different layer. The root
      of the file directory is treated as like the drupal_path, except for
      the permissions.
      
      So YOUR_DRUPAL/themes/moo/style/night
      and YOUR_DRUPAL/path_to_files/themes/moo/styles/night
      
      are treated the same.

===========================
  Tips
===========================
  - You can replace images in CSS as you wish, simple use relative locations.
  
  =====================
    Theme stylesheet architecture
  =====================
    Essentially, you can seperate styling across stylesheets to increase the
    theme's flexibility. If you are interested in a good architecture for
    stylesheets, see the Zen starter theme. (http://www.drupal.org/project/zen)
  
    If you are distributing a theme and want maximum configurability, you can
    for instance move all of your font CSS into a fonts.css stylesheet, and
    your images into a images.css stylesheet.
    
    You can also seperate configurable CSS (font, color, background) and
    non-configurable. Only the theme developer himself must determine what aspects
    of the CSS cannot be changed.
    
    Simply create two files, same statement, but different attributes.

===========================
  7.x goals
===========================
  - Overriding of .tpl.php files
  - In lieu of the stylesheet architecture tip, perhaps a core patch to improve
    Drupal's stylesheet caching system.

===========================
  Style-compatible extensions
===========================

  Harmony (to be included)
    Link module styles attach to theme styles.
  
  Color
    Create customizable stylesheets and images
      - Method of creating customizable stylesheets
      - Interface for customizing colors
      - Generation system. Output directly to native style.module styles.
  
  Big bonus: Styles will work as long as they are properly placed.
    This leaves room for developers to create custom front-ends and applications
    for customizing styles through drupal.


========================
  CHANGELOG
========================
  See CHANGELOG.TXT